package academy.learnprogramming;

import jdk.incubator.http.HttpClient;
import jdk.incubator.http.HttpHeaders;
import jdk.incubator.http.HttpRequest;
import jdk.incubator.http.HttpResponse;

import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

public class Main {
    public static void main(String[] args) throws Exception {
        String url = "http://example.com";

        getURLASync(url);
    }

    // Make an Asynchronous GET request and process the ressponse as a String

    public static void getURLASync(String url) throws Exception {
        HttpClient client = HttpClient.newHttpClient();

        CompletableFuture<HttpResponse<String>> compFuture = client.sendAsync(
                HttpRequest
                        .newBuilder(new URI(url))
                        .GET()
                        .build(),
                HttpResponse.BodyHandler.asString());

        System.out.println("Async request has been made...");

        while(!compFuture.isDone()) {
            System.out.println("Do something else while we wait...");

            /*
                if (someCondition ) {
                    compFuture.cancel(true);
                    System.out.println("Async request has been cancelled");
                }
            */
        }

        System.out.println("Async request is done...");
        processResponse(compFuture.get());
    }

    // Process the response
    // Display the status code
    // All the header data
    // and finally the response body

    public static void processResponse(HttpResponse response) {


        System.out.println("Status Code: " + response.statusCode());

        System.out.println("Headers:");

        HttpHeaders headers = response.headers();
        Map<String, List<String>> headerList = headers.map();
        headerList.forEach((k, v) -> System.out.println("\t" + k + ":" + v));

        System.out.println(response.body());
    }
}
