package academy.learnprogramming;

import java.io.Closeable;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Frank J. Mitropoulos
 */
public class Main {

    public static void main(String[] args) {
        /* this calling to the constructor includes
        an override to the processData().
        the overrode method is called in
        c1.processData().
         */
        MyClass<String> c1 = new MyClass<String>("Frank") {
            @Override
            void processData() {
                System.out.println("Processing " + getData());
            }
        };
        c1.processData();


        // Allow the diamond operator with anonymous classes
        // the type of the inferred type is denotable

        MyClass<String> c2 = new MyClass<>("James") {
            @Override
            void processData() {
                System.out.println("Processing " + getData());
            }
        };
        c2.processData();

        MyClass<Integer> c3 = new MyClass<>(1000) {
            @Override
            void processData() {
                System.out.println("Processing " + getData());
            }
        };
        c3.processData();

        /* The next statement is commented because the compiler
        doesn't accept it.
        Cannot infer the type of a non-denotable type (intersection type).
        the compiler is not able to infer the type
         */
        //   MyClass<T extends Comparable<T> & Serializable> c4 = new MyClass<> {}
    }
}
