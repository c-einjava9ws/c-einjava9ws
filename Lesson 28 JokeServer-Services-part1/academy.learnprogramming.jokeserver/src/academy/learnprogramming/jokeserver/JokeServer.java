package academy.learnprogramming.jokeserver;

//import java.sql.*;
import java.util.List;
/**
 * Created by Frank J. Mitropoulos.
 */

public interface JokeServer {
    public int availableJokes();
    public String getJoke();
    public String name();
}

