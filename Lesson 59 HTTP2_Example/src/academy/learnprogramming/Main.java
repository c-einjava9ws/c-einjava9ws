package academy.learnprogramming;

import jdk.incubator.http.HttpClient;
import jdk.incubator.http.HttpHeaders;
import jdk.incubator.http.HttpRequest;
import jdk.incubator.http.HttpResponse;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

public class Main {
    public static void main(String[] args) throws Exception {
        String url = "http://example.com";

        //first test uses getURLSync().
        //getURLSync(url);

        //second test
        postURLSync(url);
    }

    public static void getURLSync(String url) throws Exception {
        HttpClient client = HttpClient.newHttpClient();

        HttpRequest request = HttpRequest
                                .newBuilder(new URI(url))
                                .GET()
                                .build();

        HttpResponse response = client.send(request, HttpResponse.BodyHandler.asString());

        processResponse(response);
    }

    public static void processResponse(HttpResponse response) {
        System.out.println("Status code: " + response.statusCode());
        System.out.println("Body: " + response.body());
        System.out.println("Headers: " );

        HttpHeaders header = response.headers();
        Map<String, List<String>> map = header.map();
        map.forEach((k, v) -> System.out.println("\t" + k + ":" + v));

    }

    private static void postURLSync(String url) throws Exception {
        HttpClient client = HttpClient.newHttpClient();

        HttpResponse response = client.send(
                HttpRequest
                    .newBuilder(new URI(url))
                    .headers("Foo", "Foo-val", "Bar", "Bar-val")
                    .POST(HttpRequest.BodyProcessor.fromString("This is the string"))
                    .build(),
                HttpResponse.BodyHandler.asFile(Paths.get("fileXXXX.txt"))
        );

        processResponse(response);
    }
}